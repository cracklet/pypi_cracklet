# Pypi package of cRacklet

This is the short documentation on how to create a PyPi package for cRacklet

## Building the docker image

```sh
docker build -t manylinux/cracklet docker/manylinux
```

## Running the docker image

```sh
docker run -ti -v $PWD:/io  manylinux/cracklet
```

## Launch the script creating wheels

```sh
./make-wheels.sh
```

Enjoy!