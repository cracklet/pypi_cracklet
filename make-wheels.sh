#!/bin/bash
set -e -x
export PLAT=manylinux2010_x86_64
source /etc/profile
SRC=/io
if [ "x${CI_PROJECT_DIR}" != x ]; then
    SRC=${CI_PROJECT_DIR}
fi
cd /build/
cmake ${SRC} -DcRacklet_DIR=/softs
function repair_wheel {
    wheel="$1"
    if ! auditwheel show "$wheel"; then
        echo "Skipping non-platform wheel $wheel"
    else
        auditwheel repair "$wheel" --plat "$PLAT" -w dist/
    fi
}
#/opt/python/cp35-cp35m/bin/pip install -r ${SRC}/dev-requirements.txt
#/opt/python/cp35-cp35m/bin/python3 setup.py sdist
# Compile wheels
for PYBIN in /opt/python/cp3*/bin; do
    "${PYBIN}/pip" install -r ${SRC}/dev-requirements.txt
    "${PYBIN}/pip" wheel . --no-deps -w dist/
done
# Bundle external shared libraries into the wheels
for whl in dist/*.whl; do
    repair_wheel "$whl"
done
cp -r dist/ ${SRC}
